local metadata =
{
	plugin =
	{
		format = 'staticLibrary',
		staticLibs = { 'plugin.video’, },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
