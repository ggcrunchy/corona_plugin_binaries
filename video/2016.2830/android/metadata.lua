local metadata =
{
	plugin =
	{
		format = 'sharedLibrary',
		staticLibs = { 'plugin.video’, },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
