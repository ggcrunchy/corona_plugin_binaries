local metadata =
{
	plugin =
	{
		format = 'staticLibrary',
		staticLibs = { 'plugin.MemoryBlob', ‘boost’ },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
