local metadata =
{
	plugin =
	{
		format = 'staticLibrary',
		staticLibs = { 'plugin.truetype' },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
