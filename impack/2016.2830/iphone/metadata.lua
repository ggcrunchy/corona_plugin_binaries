local metadata =
{
	plugin =
	{
		format = 'staticLibrary',
		staticLibs = { 'plugin.impack', ‘boost’ },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
