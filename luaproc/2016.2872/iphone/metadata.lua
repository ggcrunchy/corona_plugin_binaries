local metadata =
{
	plugin =
	{
		format = 'staticLibrary',
		staticLibs = { 'plugin.luaproc', },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
