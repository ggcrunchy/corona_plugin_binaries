local metadata =
{
	plugin =
	{
		format = 'sharedLibrary',
		staticLibs = { 'plugin.msquares' },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
