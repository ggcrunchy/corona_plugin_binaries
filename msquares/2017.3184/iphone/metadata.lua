local metadata =
{
	plugin =
	{
		format = 'staticLibrary',
		staticLibs = { 'plugin.msquares', },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
