local metadata =
{
	plugin =
	{
		format = 'sharedLibrary',
		staticLibs = { 'plugin.object3d' },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
