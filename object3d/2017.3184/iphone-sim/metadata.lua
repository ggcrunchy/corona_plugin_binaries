local metadata =
{
	plugin =
	{
		format = 'staticLibrary',
		staticLibs = { 'plugin.object3d', },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
