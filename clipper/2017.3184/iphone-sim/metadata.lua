local metadata =
{
	plugin =
	{
		format = 'staticLibrary',
		staticLibs = { 'plugin.clipper', },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
