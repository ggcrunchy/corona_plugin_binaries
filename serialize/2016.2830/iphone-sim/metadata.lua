local metadata =
{
	plugin =
	{
		format = 'staticLibrary',
		staticLibs = { 'plugin.serialize’, },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
