local metadata =
{
	plugin =
	{
		format = 'staticLibrary',
		staticLibs = { 'plugin.freeimage’, },
		frameworks = {},
		frameworksOptional = {},
	},
}

return metadata
